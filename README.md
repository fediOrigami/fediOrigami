![](fediverse-icons/party_circled_green_48_retina.png) ![](fediverse-icons/party_circled_purple_48_retina.png) ![](fediverse-icons/party_circled_yellow_48_retina.png) ![](fediverse-icons/party_circled_blue_48_retina.png) ![](fediverse-icons/party_circled_red_48_retina.png)

**Presenting a fediverse icon ANYONE in the fediverse can use!**

Social icons on a website (or I2P eepsite) are often important to communicate where else we exist online. Previously, fedizens were left with two choices; use an inaccurate or deceptive icon based on a specific type of fediverse server, or a gaudy icon with negative connotations. Most chose the former, elevating one implementation of the social network, at the expense of ALL others. It created a false impression of fediverse's topology and had a negative effect on the fediverse generally.

Since Feb 2021, a [supporters group](https://activism.openworlds.info/@dsfgs) in collaboration with FreedCreative and others, worked on a Fediverse icon. This is the result of much feedback, review and iteration. 

This icon pack, called "#fediOrigami", is based on an origami boat turned on its side. The overlapping, semi-transparent "folds" are their own entities, but interact with each other to form an 'F' and also create a silhouette of part of a star. A star being an homage to the previous but seldom-used icon design (a connected five-pointed star).


# Fediverse icons ('party' style)

The 'party' style is for websites with a high-contrast palette. Click to enlarge below sample image.

[![](posters/fediorigami_party_display.jpg)](posters/fediorigami_party_display.png)

Before we look at the different versions of the icon, today with HD retina displays it is important to include high-definition retina versions of icons. Here they are either shown alongside each icon *or* click the icon to access it. You should be able to find the SVG version of all images easily, often (but not always) by simply removing '_retina', and by replacing 'png' with 'svg' in filename.


### 'Party' Hero icons

Minimum recomended size is 24px for these, ie. they are /not/ recommended as favicon. In this part, clicking icons opens their HD (2x) retina equivalent.

[![](fediverse-icons/party_hero_24.png)](fediverse-icons/party_hero_24_retina.png) [![](fediverse-icons/party_hero_24_28.png)](fediverse-icons/party_hero_24_28_retina.png) [![](fediverse-icons/party_hero_24_32.png)](fediverse-icons/party_hero_24_32_retina.png)

[![](fediverse-icons/party_hero_48.png)](fediverse-icons/party_hero_48_retina.png) [![](fediverse-icons/party_hero_48_64.png)](fediverse-icons/party_hero_48_64_retina.png)

See [ultra HD version](fediverse-icons/party_hero_48_2400px.png) — for backgrounds, posters and, importantly, billboards I know you'll hack 🤣.


### 'Party' In-circle

![](fediverse-icons/party_circled_purple_48.png) ![](fediverse-icons/party_circled_purple_48_retina.png) ![](fediverse-icons/party_circled_yellow_48.png) ![](fediverse-icons/party_circled_yellow_48_retina.png) ![](fediverse-icons/party_circled_blue_48.png) ![](fediverse-icons/party_circled_blue_48_retina.png)

![](fediverse-icons/party_circled_green_48.png) ![](fediverse-icons/party_circled_green_48_retina.png) ![](fediverse-icons/party_circled_red_48.png) ![](fediverse-icons/party_circled_red_48_retina.png) ![](fediverse-icons/party_circled_white_48.png) ![](fediverse-icons/party_circled_white_48_retina.png)

![](fediverse-icons/party_circled_black_48.png) ![](fediverse-icons/party_circled_black_48_retina.png) ![](fediverse-icons/party_circled_white_hero_48.png) ![](fediverse-icons/party_circled_white_hero_48_retina.png) ![](fediverse-icons/party_circled_black_hero_48.png) ![](fediverse-icons/party_circled_black_hero_48_retina.png)


### 'Party' Duotone (click these ones to access their HD retina display version)

[![](fediverse-icons/party_duotone_purple_16.png)](fediverse-icons/party_duotone_purple_16_retina.png) [![](fediverse-icons/party_duotone_purple_18.png)](fediverse-icons/party_duotone_purple_18_retina.png) [![](fediverse-icons/party_duotone_purple_22.png)](fediverse-icons/party_duotone_purple_22_retina.png) [![](fediverse-icons/party_duotone_purple_24.png)](fediverse-icons/party_duotone_purple_24_retina.png) [![](fediverse-icons/party_duotone_purple_28.png)](fediverse-icons/party_duotone_purple_28_retina.png) [![](fediverse-icons/party_duotone_purple_32.png)](fediverse-icons/party_duotone_purple_32_retina.png)

[![](fediverse-icons/party_duotone_blue_16.png)](fediverse-icons/party_duotone_blue_16_retina.png) [![](fediverse-icons/party_duotone_blue_18.png)](fediverse-icons/party_duotone_blue_18_retina.png) [![](fediverse-icons/party_duotone_blue_22.png)](fediverse-icons/party_duotone_blue_22_retina.png) [![](fediverse-icons/party_duotone_blue_24.png)](fediverse-icons/party_duotone_blue_24_retina.png) [![](fediverse-icons/party_duotone_blue_28.png)](fediverse-icons/party_duotone_blue_28_retina.png) [![](fediverse-icons/party_duotone_blue_32.png)](fediverse-icons/party_duotone_blue_32_retina.png)

[![](fediverse-icons/party_duotone_green_16.png)](fediverse-icons/party_duotone_green_16_retina.png) [![](fediverse-icons/party_duotone_green_18.png)](fediverse-icons/party_duotone_green_18_retina.png) [![](fediverse-icons/party_duotone_green_22.png)](fediverse-icons/party_duotone_green_22_retina.png) [![](fediverse-icons/party_duotone_green_24.png)](fediverse-icons/party_duotone_green_24_retina.png) [![](fediverse-icons/party_duotone_green_28.png)](fediverse-icons/party_duotone_green_28_retina.png) [![](fediverse-icons/party_duotone_green_32.png)](fediverse-icons/party_duotone_green_32_retina.png)

[![](fediverse-icons/party_duotone_red_16.png)](fediverse-icons/party_duotone_red_16_retina.png) [![](fediverse-icons/party_duotone_red_18.png)](fediverse-icons/party_duotone_red_18_retina.png) [![](fediverse-icons/party_duotone_red_22.png)](fediverse-icons/party_duotone_red_22_retina.png) [![](fediverse-icons/party_duotone_red_24.png)](fediverse-icons/party_duotone_red_24_retina.png) [![](fediverse-icons/party_duotone_red_28.png)](fediverse-icons/party_duotone_red_28_retina.png) [![](fediverse-icons/party_duotone_red_32.png)](fediverse-icons/party_duotone_red_32_retina.png)


### 'Party' Line-styles

![](fediverse-icons/party_line_16.png) ![](fediverse-icons/party_line_16_retina.png)

![](fediverse-icons/party_line_24.png) ![](fediverse-icons/party_line_24_retina.png)

![](fediverse-icons/party_line_32.png) ![](fediverse-icons/party_line_32_retina.png)

![](fediverse-icons/party_mixed_onLight_purple_48.png) ![](fediverse-icons/party_mixed_onLight_purple_48_retina.png) ![](fediverses-icons/party_mixed_onLight_blue_48.png) ![](fediverses-icons/party_mixed_onLight_blue_48_retina.png) ![](fediverse-icons/party_mixed_onLight_green_48.png) ![](fediverse-icons/party_mixed_onLight_green_48_retina.png) ![](fediverse-icons/party_mixed_onLight_red_48.png) ![](fediverse-icons/party_mixed_onLight_red_48_retina.png) (on light)

![](fediverse-icons/party_mixed_onDark_purple_48.png) ![](fediverse-icons/party_mixed_onDark_purple_48_retina.png) ![](fediverses-icons/party_mixed_onDark_blue_48.png) ![](fediverses-icons/party_mixed_onDark_blue_48_retina.png) ![](fediverse-icons/party_mixed_onDark_green_48.png) ![](fediverse-icons/party_mixed_onDark_green_48_retina.png) ![](fediverse-icons/party_mixed_onDark_red_48.png) ![](fediverse-icons/party_mixed_onDark_red_48_retina.png) (on dark)

### 'Party' Peertube

Peertube is the Fediverse's answer to YouTube. See [joinpeertube.org](https://joinpeertube.org/) for more info about it.

![](fediverse-icons/party_red_profile_32.png) ![](fediverse-icons/party_red_wide_32.png) ![](fediverse-icons/party_red_profile_32_retina.png) ![](fediverse-icons/party_red_wide_32_retina.png) ![](fediverse-icons/party_red_profile_32_48.png) ![](fediverse-icons/party_red_wide_32_48.png) ![](fediverse-icons/party_red_profile_32_48_retina.png) ![](fediverse-icons/party_red_wide_32_48_retina.png)

![](fediverse-icons/party_red_profile_32_96_retina.png) ![](fediverse-icons/party_red_wide_32_96_retina.png)


## Fediverse icons ('organic' style)

The 'organic' style is for websites with a more subdued colour palette.

[![Fediverse icon display showing the version of the icon for more websites with a more muted color palette](posters/fediorigami_organic_display.jpg)](posters/fediorigami_organic_display.png) (click image to enlarge)

(High-definition retina versions shown to the right of most icons, below. Often, you can find SVG formats by removing '_retina', and by replacing 'png' with 'svg' from URL)


#### 'Organic' Hero (min recommended size being 24px, ie. not a favicon)

![](fediverse-icons/organic_hero_24.png) ![](fediverse-icons/organic_hero_24_retina.png) ![](fediverse-icons/organic_hero_28.png) ![](fediverse-icons/organic_hero_28_retina.png) ![](fediverse-icons/organic_hero_32.png) ![](fediverse-icons/organic_hero_32_retina.png)

![](fediverse-icons/organic_hero_48.png) ![](fediverse-icons/organic_hero_48_retina.png) ![](fediverse-icons/organic_hero_64.png) ![](fediverse-icons/organic_hero_64_retina.png)


#### 'Organic' Duo-tone (clicking these take you to the HD retina versions)

[![](fediverse-icons/organic_duotone_blue_16.png)](fediverse-icons/organic_duotone_blue_16_retina.png) [![](fediverse-icons/organic_duotone_blue_18.png)](fediverse-icons/organic_duotone_blue_18_retina.png) [![](fediverse-icons/organic_duotone_blue_22.png)](fediverse-icons/organic_duotone_blue_22_retina.png) [![](fediverse-icons/organic_duotone_blue_24.png)](fediverse-icons/organic_duotone_blue_24_retina.png) [![](fediverse-icons/organic_duotone_blue_28.png)](fediverse-icons/organic_duotone_blue_28_retina.png) [![](fediverse-icons/organic_duotone_blue_32.png)](fediverse-icons/organic_duotone_blue_32_retina.png)

[![](fediverse-icons/organic_duotone_purple_16.png)](fediverse-icons/organic_duotone_purple_16_retina.png) [![](fediverse-icons/organic_duotone_purple_18.png)](fediverse-icons/organic_duotone_purple_18_retina.png) [![](fediverse-icons/organic_duotone_purple_22.png)](fediverse-icons/organic_duotone_purple_22_retina.png) [![](fediverse-icons/organic_duotone_purple_24.png)](fediverse-icons/organic_duotone_purple_24_retina.png) [![](fediverse-icons/organic_duotone_purple_28.png)](fediverse-icons/organic_duotone_purple_28_retina.png) [![](fediverse-icons/organic_duotone_purple_32.png)](fediverse-icons/organic_duotone_purple_32_retina.png)

[![](fediverse-icons/organic_duotone_green_16.png)](fediverse-icons/organic_duotone_green_16_retina.png) [![](fediverse-icons/organic_duotone_green_18.png)](fediverse-icons/organic_duotone_green_18_retina.png) [![](fediverse-icons/organic_duotone_green_22.png)](fediverse-icons/organic_duotone_green_22_retina.png) [![](fediverse-icons/organic_duotone_green_24.png)](fediverse-icons/organic_duotone_green_24_retina.png) [![](fediverse-icons/organic_duotone_green_28.png)](fediverse-icons/organic_duotone_green_28_retina.png) [![](fediverse-icons/organic_duotone_green_32.png)](fediverse-icons/organic_duotone_green_32_retina.png)


#### 'Organic' In-circle

![](fediverse-icons/organic_circled_blue_32.png) ![](fediverse-icons/organic_circled_blue_32_retina.png) ![](fediverse-icons/organic_circled_black_32.png) ![](fediverse-icons/organic_circled_black_32_retina.png) ![](fediverse-icons/organic_circled_hero_white_32.png) ![](fediverse-icons/organic_circled_hero_white_32_retina.png) ![](fediverse-icons/organic_circled_green_32.png) ![](fediverse-icons/organic_circled_green_32_retina.png) ![](fediverse-icons/organic_circled_purple_32.png) ![](fediverse-icons/organic_circled_purple_32_retina.png)

![](fediverse-icons/organic_circled_blue_48.png) ![](fediverse-icons/organic_circled_blue_48_retina.png) ![](fediverse-icons/organic_circled_black_48.png) ![](fediverse-icons/organic_circled_black_48_retina.png) ![](fediverse-icons/organic_circled_hero_white_48.png) ![](fediverse-icons/organic_circled_hero_white_48_retina.png) ![](fediverse-icons/organic_circled_green_48.png) ![](fediverse-icons/organic_circled_green_48_retina.png) ![](fediverse-icons/organic_circled_purple_48.png) ![](fediverse-icons/organic_circled_purple_48_retina.png)


#### 'Organic' Peertube

![](fediverse-icons/organic_red_profile_32.png) ![](fediverse-icons/organic_red_wide_32.png) ![](fediverse-icons/organic_red_profile_32_retina.png) ![](fediverse-icons/organic_red_wide_32_retina.png) ![](fediverse-icons/organic_red_profile_32_48.png) ![](fediverse-icons/organic_red_wide_32_48.png) ![](fediverse-icons/organic_red_profile_32_48_retina.png) ![](fediverse-icons/organic_red_wide_32_48_retina.png)

![](fediverse-icons/organic_red_profile_32_96_retina.png) ![](fediverse-icons/organic_red_wide_32_96_retina.png)


## Black-and-white

Although both black and white versions are shown below, depending on the background of this site only one set may be visible. If you have trouble seeing any selecting the area can help to expose them.

![](fediverse-icons/bnw_small_line.png) ![](fediverse-icons/bnw_small_line_retina.png) ![](fediverse-icons/white_small_line.png) ![](fediverse-icons/white_small_line_retina.png)

![](fediverse-icons/bnw_small_line_24.png) ![](fediverse-icons/bnw_small_line_24_retina.png) ![](fediverse-icons/white_small_line_24.png) ![](fediverse-icons/white_small_line_24_retina.png)

![](fediverse-icons/bnw_medium_line.png) ![](fediverse-icons/bnw_medium_line_retina.png) ![](fediverse-icons/white_medium_line.png) ![](fediverse-icons/white_medium_line_retina.png)

Clicking the following two lines of icons take you to HD retina equivalents.

[![](fediverse-icons/bnw_large_mixed_a_48.png)](fediverse-icons/bnw_large_mixed_a_48_retina.png) [![](fediverse-icons/bnw_large_mixed_b_48.png)](fediverse-icons/bnw_large_mixed_b_48_retina.png) [![](fediverse-icons/bnw_large_mixed_c_48.png)](fediverse-icons/bnw_large_mixed_c_48_retina.png) [![](fediverse-icons/white_large_mixed_a_48.png)](fediverse-icons/white_large_mixed_a_48_retina.png) [![](fediverse-icons/white_large_mixed_b_48.png)](fediverse-icons/white_large_mixed_b_48_retina.png) [![](fediverse-icons/white_large_mixed_c_48.png)](fediverse-icons/white_large_mixed_c_48_retina.png)

[![](fediverse-icons/bnw_small.png)](fediverse-icons/bnw_small_retina.png) [![](fediverse-icons/bnw_small_18.png)](fediverse-icons/bnw_small_18_retina.png) [![](fediverse-icons/bnw_small_22.png)](fediverse-icons/bnw_small_22_retina.png) [![](fediverse-icons/bnw_small_24.png)](fediverse-icons/bnw_small_24_retina.png) [![](fediverse-icons/bnw_small_28.png)](fediverse-icons/bnw_small_28_retina.png) [![](fediverse-icons/bnw_small_32.png)](fediverse-icons/bnw_small_32_retina.png) [![](fediverse-icons/white_small.png)](fediverse-icons/white_small_retina.png) [![](fediverse-icons/white_small_18.png)](fediverse-icons/white_small_18_retina.png) [![](fediverse-icons/white_small_22.png)](fediverse-icons/white_small_22_retina.png) [![](fediverse-icons/white_small_24.png)](fediverse-icons/white_small_24_retina.png) [![](fediverse-icons/white_small_28.png)](fediverse-icons/white_small_28_retina.png) [![](fediverse-icons/white_small_32.png)](fediverse-icons/white_small_32_retina.png)

#### In-circle

![](fediverse-icons/party_circled_black_32.png) ![](fediverse-icons/party_circled_black_32_retina.png) ![](fediverse-icons/party_circled_black_48.png) ![](fediverse-icons/party_circled_black_48_retina.png) ![](fediverse-icons/party_circled_white_32.png) ![](fediverse-icons/party_circled_white_32_retina.png) ![](fediverse-icons/party_circled_white_48.png) ![](fediverse-icons/party_circled_white_48_retina.png)


### About the SVGs

The SVG icons are Plain SVG but have yet to be properly optimised. The Scour (v0.31) export feature in Inkscape is broken. That project is hosted on MicrosoftGH (https://github.com/scour-project/scour) and our graphic designer was not interested in contacting a Microsoft user.


# How to Draw (poster)

[![Poster showing how to draw the Fediverse icon](posters/how-to-draw_fediverse-icon_400px.png)](posters/how-to-draw_fediverse-icon.png)


# Fedi Stickers

Click stickers for higher resolution.

[![Fediverse sticker for light surfaces](fediverse-icons/organic_hero_sticker_onLight_thumbnail.png)](fediverse-icons/organic_hero_sticker_onLight.png) [![Fediverse sticker for dark surfaces](fediverse-icons/organic_hero_sticker_onDark_thumbnail.png)](fediverse-icons/organic_hero_sticker_onDark.png)


## If you are looking for a mastodon icon for your website, you probably want to use a fediverse icon instead.

Please [inform](https://activism.openworlds.info/@dsfgs) if there is a popular style of icon that is not easily covered above.

Copyright DSFGS Copyleft GPLv3 for Fediverse AGPLv3 licensed softwares for everyone.
